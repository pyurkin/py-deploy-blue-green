import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          This is an awesome site {process.env.REACT_APP_SITE_ENV_TITLE}
        </p>
        <a
          className="App-link"
          href="https://en.wikipedia.org/wiki/Continuous_integration"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn CI
        </a>
      </header>
    </div>
  );
}

export default App;
